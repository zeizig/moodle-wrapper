# Moodle wrapper

This package wraps some commonly used Moodle API functionality. Makes Moodle plugin development using Laravel easier.

## Requirements

This package is meant to be used with Laravel.

Tested to work with Laravel 5.4.

## Set up

Clone this project into your `packages` folder.

Add this folder to your `composer.json` autoload list.

```json
"autoload": {
    // ...
    "psr-4": {
        // ...
        "Zeizig\\Moodle\\": "packages/moodle-wrapper/src/"
    }
},
```

Add the `MoodleServiceProvider` to your providers array. In `config/app.php`:

```php

'providers' => [
    // ...
    
    Zeizig\Moodle\MoodleServiceProvider::class,
]

```

Publish the configuration.

```bash
php artisan vendor:publish --provider=Zeizig\Moodle\MoodleServiceProvider
```

Modify your `config/moodle.php` file with your plugin slug and user id number postfix.

```php
return [
    'plugin_slug' => 'charon',
    'user_id_number_postfix' => '@ttu.ee'
];
```

The user id number postfix is used in TTÜ since all the id numbers for students have the @ttu.ee postfix. If you have no such thing just leave this blank.

## What's included?

#### Moodle globals in the `Zeizig\Moodle\Globals` namespace.

* Course
* Output
* Page
* User

#### Helper functions (global).

* `translate(string, module)` - wrapper for get_string, module defaults to plugin.
* `rewritePluginIntroUrls(introText, courseId)` - course id defaults to the current course (from global `$COURSE`)


#### Models

Some Eloquent models for Moodle tables.


#### Services

Some helper services, wrappers for Moodle functions etc.
